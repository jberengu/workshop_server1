const express= require('express');
var Pool= require('pg').Pool;
var bodyParser= require('body-parser');


const app= express();
var config= {
	host: 'localhost',
	user: 'workshopmanager',
	password: 'g~N]mD:>(9y2A,5S',
	database: 'workshops',
};

var pool= new Pool(config);

app.set('port', (8080));
app.use(bodyParser.json({type: 'application/json'}));
app.use(bodyParser.urlencoded({extended: true}));

app.get('/api', async (req, res) => {
	var request= req.query.workshop;
	try{
		var response = await pool.query('select distinct name from workshop');
		var workshopnames= response.rows.map(function (item){
				return item.name;
			});
		if (request== undefined){
			res.json ({workshop: workshopnames});
		}else{
			var check=false;
			for(i=0; i < workshopnames.length; i++){
				if(request == workshopnames[i]){
					check= true;
					var response2 = await pool.query('select attendee from workshop where name=$1', [request]);
					console.log(response2);
					var attendees= response2.rows.map(function (item){
						return item.attendee;
					
				})
				res.json ({attendees: attendees});
			}
		}
			if (!check){
				res.json({error: "workshop not found"});
			}
		}
	}
	catch(e) {
		console.error('Error running query '+ e);
	}
});

app.post('/api', async (req, res) => {
	console.log(req.body);
	var workshop= req.body.workshop;
	var attendee= req.body.attendee;
	if (!workshop || !attendee){
		res.json({error: 'parameters not given'});
	}
	else {
		try{
			var check = await pool.query('select attendee from workshop where name=$1 and attendee=$2'
				, [workshop,attendee]);
			var person=check.rows.map(function (item){
						return item.attendee;	
				});
			console.log(person);
			if(person== attendee){
				res.json({error: 'attendee already enrolled'});
			}
			else{
				var response = await pool.query('insert into workshop values($1, $2)',
					[attendee, workshop]);
				res.json({attendee: attendee, workshop: workshop});
			}
			
		}
		catch(e){
			console.log('Error running insert', e);
		}
	}
});

app.listen(app.get('port'), () => {
	console.log('Running');
})










