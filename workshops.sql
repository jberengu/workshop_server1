create database workshops;
\c workshops

drop table if exists workshop;
create table workshop (
	attendee text,
	name text
);

insert into workshop (name,attendee) values
	('DevOps 101','Jose Araujo'),
	('Docker Container Fundamentals','Jose Araujo'),
	('Machine Learning','Jose Araujo'),
	('Modern Javascript','Jose Araujo'),
	('MongoDB','Jose Araujo'),
	('React Fundamentals','Jose Araujo'),
	('Self-Driving Cars','Jose Araujo'),
	('React Fundamentals', 'Ahmed Abdelali'),
	('React Fundamentals', 'Ann Frank'),
	('React Fundamentals', 'Ann Mulkern'),
	('React Fundamentals', 'Clara Weick'),
	('React Fundamentals', 'James Archer'),
	('React Fundamentals', 'Linda Park'),
	('React Fundamentals', 'Lucy Smith'),
	('React Fundamentals', 'Roz Billingsley'),
	('React Fundamentals', 'Samantha Eggert'),
	('React Fundamentals', 'Tim Smith');

drop role if exists workshopmanager;
create user workshopmanager with password 'g~N]mD:>(9y2A,5S';
Grant select, insert on workshop to workshopmanager;




